using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// Used to load all relevants scene and elements to the game.
    /// </summary>
    public class InitGame : MonoBehaviour
    {
        [SerializeField] int initSceneId = 0;
        [SerializeField] int characterSceneId;
        [SerializeField] List<int> scenesToAddIds = new List<int>();

        async void Start()
        {
            await ActiveGameplayScenes();

            SceneManager.UnloadSceneAsync(initSceneId);
        }

        async Task ActiveGameplayScenes() 
        {
            var loadTasks = InitLoadOfScenes();

            await Task.WhenAll(loadTasks);

            foreach (var loadTask in loadTasks)
            {
                loadTask.Result.allowSceneActivation = true;
                while (!loadTask.Result.isDone)
                {
                    await Task.Delay(1);
                }
            }
        }
        Task<AsyncOperation>[] InitLoadOfScenes()
        {
            var tasks = new List<Task<AsyncOperation>>();
            foreach (var sceneId in scenesToAddIds)
            {
                tasks.Add(LoadSceneTask(sceneId, LoadSceneMode.Additive));
            }

            tasks.Add(LoadSceneTask(characterSceneId, LoadSceneMode.Additive));

            return tasks.ToArray();
        }
        async Task<AsyncOperation> LoadSceneTask(int sceneToLoad, LoadSceneMode activationMode)
        {
            var task = SceneManager.LoadSceneAsync(sceneToLoad, activationMode);
            task.allowSceneActivation = false;

            while (task.progress == 1)
            {
                await Task.Delay(1);
            }

            return task;
        }
    }
}
