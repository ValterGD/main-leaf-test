using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// ScriptableObject that holds an value or reference of type.
    /// </summary>
    /// <typeparam name="T">Type of the value stored</typeparam>
    public abstract class DataValue<T> : ScriptableObject
    {
        public T value;

        public static implicit operator T(DataValue<T> data)
        {
            return data.value;
        }
    }
}