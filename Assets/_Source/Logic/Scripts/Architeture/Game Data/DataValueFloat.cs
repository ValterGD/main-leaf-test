﻿using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// ScriptableObject that holds a float value.
    /// </summary>
    [CreateAssetMenu(fileName = "data_float_", menuName = "MainLeafTest/Game Datas/Float Data")]
    public class DataValueFloat : DataValue<float> { }
}