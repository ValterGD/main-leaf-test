﻿using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// ScriptableObject that holds an GameObject reference.
    /// </summary>
    [CreateAssetMenu(fileName = "d_gameObject_", menuName = "MainLeafTest/Game Datas/GameObject Data")]
    public class DataValueGameObject : DataValue<GameObject> { }
}