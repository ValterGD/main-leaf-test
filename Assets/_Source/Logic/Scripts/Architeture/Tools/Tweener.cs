﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace MainLeafTest.Tools
{
    ///<summary>
    ///Responsible for creating custom animation tweens from values of unity components.
    ///</summary>
    public static class Tweener
    {
        static Dictionary<Component, List<CancellationTokenSource>> currentCancelableTweens = new Dictionary<Component, List<CancellationTokenSource>>();

        /// <summary>
        /// Run a custom routine at the specified time.
        /// </summary>
        /// <param name="tweenTarget"></param>
        /// Reference on scene that will receive the routine. Reference info used to stop tweens correctly.
        /// <param name="routineInteraction"></param>
        /// Main action peformed during the routine, use with lerp methods to create a custom tween.
        /// <param name="duration"></param>
        /// Duration to complete the routine.
        /// <param name="onComplete"></param>
        /// Callback action when the routine is finished.
        /// <returns></returns>
        public static Task TweenAction(this Component tweenTarget, IProgress<float> routineInteraction, float duration, Action onComplete = null) 
        {
            var cancelator = new CancellationTokenSource();
            var task = TweenTask(tweenTarget, duration, routineInteraction, cancelator, onComplete);
            CacheCancelationToken(tweenTarget, cancelator);
            return task;
        }

        private static async Task TweenTask(Component tweenTarget, float duration, IProgress<float> progressAction, CancellationTokenSource tokenSource, Action onComplete) 
        {
            await Task.Delay(1);
            float elapsedTime = 0f;
            float lerp = 0f;

            while (lerp < 1 && duration > 0 && CheckIfCanTween(tweenTarget, tokenSource.Token))
            {
                elapsedTime += (Time.deltaTime);
                lerp = Mathf.InverseLerp(0, duration, elapsedTime);
                progressAction.Report(lerp);
                await Task.Delay(1);
            }

            if (CheckIfCanTween(tweenTarget, tokenSource.Token) == false)
            {
                progressAction = null;
                return;
            }

            progressAction.Report(1);
            onComplete?.Invoke();
        }
        private static bool CheckIfCanTween(Component tweenTarget, CancellationToken token) 
        {
            bool canTween = Application.isPlaying && tweenTarget.gameObject.activeInHierarchy && token.IsCancellationRequested == false;

#if UNITY_EDITOR
            canTween = canTween && UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode != false;
#endif
            return canTween;
        }
        private static void CacheCancelationToken(Component keyObject, CancellationTokenSource taskCancelator)
        {
            if (currentCancelableTweens.ContainsKey(keyObject) == false)
            {
                currentCancelableTweens.Add(keyObject, new List<CancellationTokenSource>());
            }

            currentCancelableTweens[keyObject].Add(taskCancelator);
        }

        /// <summary>
        /// Stop all current interpolations associated with an object.
        /// </summary>
        /// <param name="keyObject">
        /// Reference object that all interpolations will be stopped.
        /// </param>
        public static void StopAllTweens(this Component keyObject)
        {
            if (currentCancelableTweens.ContainsKey(keyObject))
            {
                foreach (var cancelator in currentCancelableTweens[keyObject])
                {
                    cancelator.Cancel();
                }

                currentCancelableTweens.Remove(keyObject);
            }

        }

    }

}
