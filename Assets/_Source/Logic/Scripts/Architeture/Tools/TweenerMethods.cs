using System;
using System.Threading.Tasks;
using UnityEngine;

namespace MainLeafTest.Tools
{
    public static class TweenerMethods
    {
        /// <summary>
        /// Moves the object to desired global position based on speed.
        /// </summary>
        /// <param name="tweenTarget"></param>
        /// Object on scene that will be moved.
        /// <param name="targetPosition"></param>
        /// Target global position to move.
        /// <param name="speed"></param>
        /// Speed of the moviment.
        /// <param name="onComplete"></param>
        /// Callback action when the moviment is finished.
        /// <returns></returns>
        public static Task TweenMove(this Transform tweenTarget, Vector3 targetPosition, float speed, Action onComplete = null)
        {
            float distance = Vector3.Distance(tweenTarget.position, targetPosition);
            float duration = distance / speed;
            return TweenPosition(tweenTarget, targetPosition, duration, onComplete);
        }

        /// <summary>
        /// Moves the object to desired global position during the specified time.
        /// </summary>
        /// <param name="tweenTarget"></param>
        /// Object on scene that will be moved.
        /// <param name="targetPosition"></param>
        /// Target global position to move.
        /// <param name="duration"></param>
        /// Duration of the moviment.
        /// <param name="onComplete"></param>
        /// Callback action when the moviment is finished.
        /// <returns></returns>
        public static Task TweenPosition(this Transform tweenTarget, Vector3 targetPosition, float duration, Action onComplete = null)
        {
            Vector3 initialPosition = tweenTarget.position;
            var progressAction = new Progress<float>(progressValue => tweenTarget.position = Vector3.Lerp(initialPosition, targetPosition, progressValue));
            return Tweener.TweenAction(tweenTarget, progressAction, duration, onComplete);
        }

        /// <summary>
        /// Rotates the object to desired local rotation during the specified time.
        /// </summary>
        /// <param name="tweenedTarget"></param>
        /// Object on scene that will be rotated.
        /// <param name="targetRotation"></param>
        /// Target final local rotatation.
        /// <param name="duration"></param>
        /// Duration of the rotation.
        /// <param name="onComplete"></param>
        /// Callback action when the moviment is finished.
        /// <returns></returns>
        public static Task TweenRotation(this Transform tweenedTarget, Quaternion targetRotation, float duration, Action onComplete = null)
        {
            Quaternion initialRotation = tweenedTarget.transform.localRotation;
            var progressAction = new Progress<float>(progressValue => tweenedTarget.transform.localRotation = Quaternion.Lerp(initialRotation, targetRotation, progressValue));
            return Tweener.TweenAction(tweenedTarget, progressAction, duration, onComplete);
        }

        /// <summary>
        /// Fades canvas group to target alpha value
        /// </summary>
        /// <param name="tweenTarget"></param>
        /// Object on scene that will be faded.
        /// <param name="targetAlpha"></param>
        /// Target final alpha value.
        /// <param name="duration"></param>
        /// Duration of the fading.
        /// <param name="onComplete"></param>
        /// Callback action when the moviment is finished.
        /// <returns></returns>
        public static Task TweenAlpha(this CanvasGroup tweenTarget, float targetAlpha, float duration, Action onComplete = null)
        {
            float initialAlpha = tweenTarget.alpha;
            var progressAction = new Progress<float>(progressValue => tweenTarget.alpha = Mathf.Lerp(initialAlpha, targetAlpha, progressValue));
            return Tweener.TweenAction(tweenTarget, progressAction, duration, onComplete);
        }
    }
}
