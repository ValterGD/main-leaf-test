﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MainLeafTest.Tools
{
    /// <summary>
    /// Responsabile to run unity coroutines outside from MonoBehaviors.
    /// </summary>
    public class GlobalCoroutiner : MonoBehaviour
    {
        private static GlobalCoroutiner _instance;
        private static GlobalCoroutiner instance => GetInstance();
        private static Dictionary<object, List<CoroutineHandler>> runningCoroutines = new Dictionary<object, List<CoroutineHandler>>();

        void OnDisable()
        {
            _instance = null;
        }

        public static GlobalCoroutiner GetInstance()
        {
            if (_instance == null)
            {
                GlobalCoroutiner newInstance = new GameObject("TweenerCoroutiner").AddComponent<GlobalCoroutiner>();
                _instance = newInstance;
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }

        /// <summary>
        /// Starts a new coroutine outisde an MonoBehavior.
        /// </summary>
        /// <param name="keyObject">
        /// Key object resposibile to call the routine.
        /// </param>
        /// <param name="originalIEnumerator">
        /// IEnumerator that'll be started.
        /// </param>
        /// On routine finished callback event.
        /// <param name="onComplete"></param>
        public static void StartCoroutine(object keyObject, IEnumerator originalIEnumerator, UnityAction onComplete = null)
        {
            CoroutineHandler handler = new CoroutineHandler();
            handler.keyObject = keyObject;
            handler.onCompleteAction = onComplete;
            handler.iEnumerator = originalIEnumerator;
            handler.runningCoroutine = instance.StartCoroutine(NewCoroutine(handler));
            CacheCoroutine(keyObject, handler);
        }
        private static IEnumerator NewCoroutine(CoroutineHandler handler)
        {
            yield return handler.iEnumerator;
            OnCoroutineCompleted(handler);
        }
        private static void OnCoroutineCompleted(CoroutineHandler handler)
        {
            handler.onCompleteAction?.Invoke();
            if (runningCoroutines[handler.keyObject]?.Contains(handler) ?? false)
            {
                runningCoroutines[handler.keyObject].Remove(handler);
            }
        }

        private static void CacheCoroutine(object keyObject, CoroutineHandler coroutine)
        {
            if (runningCoroutines.ContainsKey(keyObject) == false)
            {
                runningCoroutines.Add(keyObject, new List<CoroutineHandler>());
            }

            runningCoroutines[keyObject].Add(coroutine);
        }

        /// <summary>
        /// Stop all current global coroutines associated with an object.
        /// </summary>
        /// <param name="keyObject">
        /// Reference object that all global coroutines will be stopped.
        /// </param>
        public static void StopAllGlobalCoroutineFrom(object keyObject)
        {
            if (runningCoroutines.ContainsKey(keyObject))
            {
                foreach (var handle in runningCoroutines[keyObject])
                {
                    instance.StopCoroutine(handle.runningCoroutine);
                }

                runningCoroutines.Remove(keyObject);
            }
        }

        /// <summary>
        /// Struct used to store an running global coroutine information.
        /// </summary>
        public struct CoroutineHandler
        {
            public object keyObject;
            public IEnumerator iEnumerator;
            public Coroutine runningCoroutine;
            public UnityAction onCompleteAction;
        }
    }
}
