using UnityEngine;
using UnityEngine.Events;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// Listener that keeps an UnityEvent to be executed from an activation of a especified global event.
    /// </summary>
    public class GlobalEventListener : MonoBehaviour
    {
        [Header("Global Event")]
        [SerializeField] GlobalEvent eventToListen;
        [SerializeField] UnityEvent onEvent;

#if UNITY_EDITOR
        [Space]
        [SerializeField][TextArea] string notes = "Global Event Notes";
        [SerializeField] bool debugEvent = false;
#endif

        private void OnEnable()
        {
            eventToListen.AddListener(this);
        }

        private void OnDisable()
        {
            eventToListen.RemoveListener(this);
        }

        /// <summary>
        /// Run the defined UnityEvent.
        /// </summary>
        public void OnEventRaised()
        {
#if UNITY_EDITOR
            if (debugEvent)
            {
                Debug.Log($"Listening Event: {eventToListen}\nNotes :{notes}", gameObject);
            }
#endif
            onEvent?.Invoke();
        }
    }
}