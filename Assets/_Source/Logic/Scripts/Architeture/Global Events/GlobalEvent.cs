using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// ScriptableObject that can be activated with Invoke() anywhere that run behaviors from active listeners to this event on the scene.
    /// </summary>
    [CreateAssetMenu(fileName = "e_", menuName = "MainLeafTest/Global Event", order = 0)]
    public class GlobalEvent : ScriptableObject
    {
#if UNITY_EDITOR
        [SerializeField] bool debugEvent;
#endif
        private List<GlobalEventListener> listeners = new List<GlobalEventListener>();

        /// <summary>
        /// Run behaviors from active listeners to this event on the scene.
        /// </summary>
        public void Invoke()
        {
#if UNITY_EDITOR
            if (debugEvent)
            {
                Debug.Log($"Calling Event: {name} with {listeners.Count} listeners!");
            }
#endif
            foreach (var listener in listeners)
            {
                listener.OnEventRaised();
            }
        }

        /// <summary>
        /// Run behaviors from active listeners to this event on the scene.
        /// </summary>
        /// <param name="time">
        /// Wait time to invoke this event
        /// </param>
        public void DelayedInvoke(float time)
        {
            _ = DelayTask(time);
        }

        /// <summary>
        /// Run behaviors at next frame from active listeners to this event on the scene.
        /// </summary>
        public async void InvokeNextFrame()
        {
            await DelayTask(0.001f);
        }
        private async Task DelayTask(float time)
        {
#if UNITY_EDITOR
            if (debugEvent)
            {
                Debug.Log($"Delay Call of Event: {name}, in {time} seconds!");
            }
#endif
            int miliseconds = Mathf.RoundToInt(time * 1000);
            await Task.Delay(miliseconds);
            Invoke();
        }

        /// <summary>
        /// Adds an listener to this event.
        /// </summary>
        public void AddListener(GlobalEventListener listener)
        {
            listeners.Add(listener);
        }

        /// <summary>
        /// Removes an listener to this event.
        /// </summary>
        public void RemoveListener(GlobalEventListener listener)
        {
            listeners.Remove(listener);
        }
    }
}