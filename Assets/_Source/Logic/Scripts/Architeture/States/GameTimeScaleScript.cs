﻿using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// Game stat that keeps an defined TimeScale execution. Restore TimeScale to 1 on StateExit. 
    /// </summary>
    [CreateAssetMenu(fileName = "gState_", menuName = "MainLeafTest/Game States/Time Scaler")]
    public class GameTimeScaleScript : GameStateScript
    {
        [Header("Time")]
        [SerializeField][Range(0, 1)] float scaleDuringState = 1;

        public override void OnGameStateEnter()
        {
            base.OnGameStateEnter();
            Time.timeScale = scaleDuringState;
        }

        public override void OnGameStateExit()
        {
            base.OnGameStateExit();
            Time.timeScale = 1;
        }
    }
}
