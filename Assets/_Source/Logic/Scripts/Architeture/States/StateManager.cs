using System.Collections.Generic;
using UnityEngine;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// Manager the current game state, call events from game state changing and defines the first state of the game. Disable game cursor on game load if is needed.
    /// </summary>
    public class StateManager : MonoBehaviour
    {
        [SerializeField] GameStateScript initState;
        [SerializeField] bool disableCursorOnLoad = true;

        protected static GameStateScript currentState;
        public static GameStateScript state => currentState;

        private void Awake()
        {
            if (disableCursorOnLoad)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        private void Start()
        {
            ChangeGameState(initState);
        }

        /// <summary>
        /// Set the selected game state to be the current state. Calling the OnExit events from the older game stat and OnEnter events from the new game stat.
        /// </summary>
        /// <param name="nextState">
        /// Game state to be activated.
        /// </param>
        public static void ChangeGameState(GameStateScript nextState)
        {
#if UNITY_EDITOR && false
        Debug.Log($"Changing State | {currentState} ==> {nextState}");
#endif
            if (nextState == currentState)
            {
                return;
            }

            currentState?.OnGameStateExit();
            currentState = nextState;
            currentState.OnGameStateEnter();
        }
    }
}