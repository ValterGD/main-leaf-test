﻿using System.Collections;
using UnityEngine;
using MainLeafTest.Tools;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// Intermediary game stat responsible to transits the game to another state during the specified time. Can call fade in event if is needed.
    /// </summary>
    [CreateAssetMenu(fileName = "gState_transition", menuName = "MainLeafTest/Game States/Transition")]
    public class GameTransitionScript : GameStateScript
    {
        [Header("Transition")]
        [SerializeField] float beginTransitionTime = 2;
        [Header("Fading Setup")]
        [SerializeField] DataValueFloat fadeTime;
        [SerializeField] GlobalEvent fadeEvent;
        [SerializeField] float waitAfterFade = 0.5f;
        [Header("States Setup")]
        [SerializeField] GameStateScript onEndNextState;

        public override void OnGameStateEnter()
        {
            base.OnGameStateEnter();
            GlobalCoroutiner.StartCoroutine(this, TransitionBehavior());
        }
        IEnumerator TransitionBehavior()
        {
            yield return new WaitForSeconds(beginTransitionTime);
            fadeEvent?.Invoke();
            yield return new WaitForSeconds(fadeTime);
            yield return new WaitForSeconds(waitAfterFade);
            onEndNextState?.ActivateState();
        }
    }
}