﻿using UnityEngine;
using UnityEngine.Events;

namespace MainLeafTest.Architecture
{
    /// <summary>
    /// ScriptableObject used to transition between game states in a virtual state machine during gameplay.
    /// </summary>
    [CreateAssetMenu(fileName = "gState_", menuName = "MainLeafTest/Game States/Generic")]
    public class GameStateScript : ScriptableObject
    {
        [SerializeField][Tooltip("This global event is called before this OnEnterUnityEvents")] 
        GlobalEvent onStateEnterEvent;
        [SerializeField][Tooltip("This global event is called before this OnExitUnityEvents")] 
        GlobalEvent onStateExitEvent;
        [Space]
        [SerializeField] UnityEvent onStateEnterUnityEvents;
        [SerializeField] UnityEvent onStateExitUnityEvents;

        public GameStateScript() { }


        /// <summary>
        /// Set this game state to be the current state. Calling the OnExit events from the older game stat and OnEnter events from this game stat.
        /// </summary>
        public virtual void ActivateState()
        {
            StateManager.ChangeGameState(this);
        }

        public virtual void OnGameStateEnter()
        {
            onStateEnterEvent?.Invoke();
            onStateEnterUnityEvents?.Invoke();
        }
        public virtual void OnGameStateExit()
        {
            onStateExitEvent?.Invoke();
            onStateExitUnityEvents?.Invoke();
        }
    }
}