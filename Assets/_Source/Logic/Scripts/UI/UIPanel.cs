﻿using UnityEngine;
using MainLeafTest.Architecture;
using MainLeafTest.Tools;

namespace MainLeafTest.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIPanel : MonoBehaviour
    {
        [Header("UI Settings")]
        [SerializeField] bool isInteractable = true;
        [SerializeField] bool autoDisable = true;

        protected CanvasGroup cg;

        protected virtual void Awake()
        {
            cg = GetComponent<CanvasGroup>();

            if(autoDisable)
                ShowPanel(false);
        }

        public void ShowPanel(bool show)
        {
            if (isInteractable)
                EnableMouse(show);

            bool canInteract = isInteractable && show;
            cg.interactable = canInteract;
            cg.blocksRaycasts = canInteract;
            cg.alpha = show ? 1 : 0;
        }

        private void EnableMouse(bool enable)
        {
            Cursor.lockState = enable ? CursorLockMode.None : CursorLockMode.Locked;
            Cursor.visible = enable;
        }

        public void FadeOut(DataValueFloat time)
        {
            FadePanel(fadeIn: false, time);
        }
        public void FadeIn(DataValueFloat time)
        {
            FadePanel(fadeIn: true, time);
        }

        public void FadePanel(bool fadeIn, float time)
        {
            cg.alpha = fadeIn ? 0 : 1;
            cg.TweenAlpha((fadeIn ? 1 : 0), time);
        }

    }
}
