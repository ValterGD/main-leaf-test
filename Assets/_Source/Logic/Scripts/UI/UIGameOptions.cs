using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MainLeafTest.Architecture;

namespace MainLeafTest.UI
{
    public class UIGameOptions : UIPanel
    {
        [Header("Camera Sensibility")]
        [SerializeField] Slider sensibilitySlider;
        [SerializeField] DataValueFloat cameraSensibility;
        [SerializeField] Vector2 minMaxSensibility;
        [Header("Exit Pause Game")]
        [SerializeField] GameStateScript gamePlayState;
        [SerializeField] GameStateScript resetState;
        [SerializeField] GlobalEvent onOptionsApply;


        protected override void Awake()
        {
            base.Awake();
            LoadSensibility();
            sensibilitySlider.onValueChanged.AddListener(SetSensibility);
        }

        private void LoadSensibility()
        {
            sensibilitySlider.value = Mathf.InverseLerp(minMaxSensibility.x, minMaxSensibility.y, cameraSensibility.value);
        }

        private void SetSensibility(float sensibility)
        {
            cameraSensibility.value = Mathf.Lerp(minMaxSensibility.x, minMaxSensibility.y, sensibility);
        }

        public void ResumeGame()
        {
            gamePlayState.ActivateState();
        }

        public void PauseGame(bool pause)
        {
            ShowPanel(pause);

            if (!pause)
            {
                onOptionsApply?.Invoke();
            }
        }

        public void RestartGame()
        {
            resetState?.ActivateState();
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }

    }
}