using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using MainLeafTest.Architecture;

namespace MainLeafTest
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] GameStateScript pauseState;
        [SerializeField] GameStateScript gameplayState;

        PlayerInputActions inputActions;
        PlayerController playerController;

        private void Awake()
        {
            inputActions = new PlayerInputActions();
            playerController = GetComponent<PlayerController>();
        }

        private void OnEnable()
        {
            inputActions.UI.Enable();
            inputActions.UI.Cancel.performed += PauseAction;
            inputActions.Player.Jump.performed += JumpCommand;
            inputActions.Player.Fire.performed += InteractCommand;
            inputActions.Player.Fire.canceled += InteractCommand;
            inputActions.Player.Crouch.performed += CrouchCommand;
            inputActions.Player.Crouch.canceled += CrouchCommand;
        }

        private void OnDisable()
        {
            inputActions.UI.Cancel.performed -= PauseAction;
            inputActions.Player.Jump.performed -= JumpCommand;
            inputActions.Player.Fire.performed -= InteractCommand;
            inputActions.Player.Fire.canceled -= InteractCommand;
            inputActions.Player.Crouch.performed -= CrouchCommand;
            inputActions.Player.Crouch.canceled -= CrouchCommand;
        }

        public void EnableGameplayInputs(bool enable)
        {
            if (enable)
            {
                inputActions.Player.Enable();
            }
            else
            {
                inputActions.Player.Disable();
            }

        }

        private void PauseAction(InputAction.CallbackContext obj)
        {
            if (StateManager.state == gameplayState)
                pauseState?.ActivateState();
            else if (StateManager.state == pauseState)
            {
                gameplayState?.ActivateState();
            }
        }

        private void JumpCommand(InputAction.CallbackContext callback)
        {
            playerController.Jump();
        }

        private void InteractCommand(InputAction.CallbackContext callback)
        {
            switch (callback.action.phase)
            {
                case InputActionPhase.Started:
                case InputActionPhase.Performed:
                    playerController.Interact(true);
                    break;
                case InputActionPhase.Canceled:
                    playerController.Interact(false);
                    break;
            }
        }

        private void CrouchCommand(InputAction.CallbackContext callback)
        {
            switch (callback.action.phase)
            {
                case InputActionPhase.Started:
                case InputActionPhase.Performed:
                    playerController.Crouch(true);
                    break;
                case InputActionPhase.Canceled:
                    playerController.Crouch(false);
                    break;
            }
        }

        private void Update()
        {
            Vector2 movimentValue = inputActions.Player.Move.ReadValue<Vector2>();
            playerController.Move(movimentValue);
        }
    }
}
