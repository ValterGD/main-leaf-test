using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;
using UnityEngine.InputSystem.XR;

namespace MainLeafTest
{
    [RequireComponent(typeof(PlayerMovimentSkill))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] Animator useAnimator;
        [Space]
        [SerializeField] DataValueGameObject playerReference;

        PlayerMovimentSkill mover;
        PlayerDragSkill dragger;

        Vector2 currentMoveInput;

        private void Awake()
        {
            mover = GetComponent<PlayerMovimentSkill>();
            dragger = GetComponent<PlayerDragSkill>();
        }

        private void OnEnable()
        {
            playerReference.value = gameObject;
        }

        public void Move(Vector2 moviment) 
        {
            currentMoveInput = moviment;
        }

        public void Interact(bool startInteraction = true) 
        {
            dragger?.GrabInteraction(startInteraction);
        }

        public void Jump() 
        {
            mover.Jump();
        }

        public void Crouch(bool crouch) 
        {
            mover.Crouch(crouch);
        }

        private void Update()
        {
            if (dragger?.isGrabbing == false)
            {
                mover.SetSpeed(fowardValue: currentMoveInput.y, torqueValue: currentMoveInput.x);
            }
            else 
            {
                dragger.SetFowardForce(currentMoveInput.y);
                mover.SetSpeed(0, 0);
            }
        }

        private void LateUpdate()
        {
            if (useAnimator != null)
            {
                float onGroundSpeed = new Vector2(mover.currentMoviment.x, mover.currentMoviment.z).magnitude * (mover.isGrounded ? 1 : 0);

                useAnimator.SetFloat("groundSpeed", onGroundSpeed);
                useAnimator.SetFloat("fwDirection", currentMoveInput.y);
                useAnimator.SetBool("isGrounded", mover.isGrounded);
                useAnimator.SetBool("isCrouch", mover.isCrouching);
            }

        }

    }

}
