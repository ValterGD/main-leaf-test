using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MainLeafTest
{
    public class RuppiePocket : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI ruppieCount;

        int currentRuppies = 0;

        public void AddRuppie(int qnt)
        {
            currentRuppies += qnt;
            UpdateUI();
        }

        public void UpdateUI()
        {
            string countTxt = $"x{((currentRuppies / 10) < 1 ? "0" : "")}{currentRuppies}";
            ruppieCount.text = countTxt;
        }

        public void ClearPocket()
        {
            currentRuppies = 0;
            UpdateUI();
        }
    }
}