using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;
using MainLeafTest.Tools;

namespace MainLeafTest
{
    public class PlayerDragSkill : MonoBehaviour
    {
        [Header("Positioning")]
        [SerializeField] float timeToSetPosition = 0.2f;
        [SerializeField] float positionDistance = 0.2f;
        [Header("Pulling")]
        [SerializeField] float dragSpeed = 3;
        [SerializeField] float pullDuration = 0.5f;
        [SerializeField] float pullInterval = 1;
        [Header("Box Reference")]
        [SerializeField] LayerMask draggables;
        [SerializeField] DataValueGameObject dragItemReference;
        [Header("DragEvents")]
        [SerializeField] GlobalEvent onObjectGrab;
        [SerializeField] GlobalEvent onObjectRelease;

        Rigidbody currentDragRB;
        CharacterController controller;

        public bool isGrabbing => grabbingBox;
        public bool canDrag => dragItemReference?.value;

        float currentFowardSpeed;
        bool grabbingBox = false;
        bool doingForce = false;
        bool pulling = false;

        private void Awake()
        {
            controller = GetComponent<CharacterController>();
            dragItemReference.value = null;
        }
 
        public void GrabInteraction(bool grab) 
        {
            if (grab != grabbingBox)
            {
                if (grab && canDrag)
                {
                    GrabElement();
                }
                else 
                {
                    ReleaseElement();
                }
            }
        }
        void GrabElement()
        {
            if (canDrag)
            {
                grabbingBox = true;
                currentDragRB = dragItemReference.value.GetComponent<Rigidbody>();
                onObjectGrab?.Invoke();
                MoveToGrabPosition();
            }
        }
        void ReleaseElement()
        {
            StopAllCoroutines();
            transform.StopAllTweens();
            grabbingBox = false;
            doingForce = false;
            pulling = false;
            currentDragRB = null;
            controller.enabled = true;
            onObjectRelease?.Invoke();
        }

        public void SetFowardForce(float fowardSpeed)
        {
            if (grabbingBox)
            {
                currentFowardSpeed = fowardSpeed;
                bool applyForce = currentFowardSpeed != 0;
                ApplyPullingForce(applyForce);
            }
        }
        void ApplyPullingForce(bool applyForce)
        {
            if (applyForce != doingForce)
            {
                doingForce = applyForce;

                if (applyForce)
                {
                    StartCoroutine(PullRoutine());
                }
                else
                {
                    StopAllCoroutines();
                }
            }
        }
        IEnumerator PullRoutine()
        {
            while (doingForce)
            {
                pulling = false;
                yield return new WaitForSeconds(pullInterval);
                pulling = true;
                yield return new WaitForSeconds(pullDuration);
            }
        }

        private void FixedUpdate()
        {
            if (pulling)
            {
                Vector3 pullingValue = ((transform.forward * currentFowardSpeed) * dragSpeed);
                pullingValue *= Time.fixedDeltaTime;

                if (currentFowardSpeed >= 0)
                {
                    currentDragRB.transform.position += pullingValue;
                    controller.Move(pullingValue);
                }
                else
                {
                    controller.Move(pullingValue);
                    currentDragRB.transform.position += pullingValue;
                }
            }
        }

        void MoveToGrabPosition()
        {
            RaycastHit hit = GetGrabPoint();

            controller.enabled = false;
            transform.TweenRotation(GetRotationToGrab(hit), timeToSetPosition);
            transform.TweenPosition(GetPositionToGrab(hit), timeToSetPosition, () => controller.enabled = true);
        }
        RaycastHit GetGrabPoint()
        {
            Vector3 dir = (dragItemReference.value.transform.position - transform.position).normalized;
            List<RaycastHit> hits = new List<RaycastHit>(Physics.RaycastAll(transform.position, dir, 1, draggables));
            return hits.Find(hit => hit.collider.CompareTag("Draggable"));
        }
        Quaternion GetRotationToGrab(RaycastHit GrabHit)
        {
            float angle = Quaternion.LookRotation(GrabHit.normal, Vector3.up).eulerAngles.y;
            Quaternion targetRotation = Quaternion.Euler(new Vector3(transform.eulerAngles.x, angle + 180, transform.eulerAngles.z));

            return targetRotation;
        }
        Vector3 GetPositionToGrab(RaycastHit GrabHit)
        {
            Vector3 hitNormal = GrabHit.normal;
            hitNormal.y = 0;
            Vector3 targetPosition = GrabHit.point + hitNormal * positionDistance;
            targetPosition = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);

            return targetPosition;
        }
    }
}
