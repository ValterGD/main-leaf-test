using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.PlayerLoop;

namespace MainLeafTest
{
    public class PlayerMovimentSkill : MonoBehaviour
    {
        [Header("Moviment")]
        [SerializeField] float movimentSpeed = 10;
        [SerializeField][Range(0, 1)] float backWalkModf = 0.08f;
        [SerializeField] float turnSpeed = 2;
        [Header("On Air")]
        [SerializeField] float jumpStrength = 2;
        [SerializeField][Range(0, 1)] float onAirControll = 0.5f;
        [Header("Crouch")]
        [SerializeField][Range(0, 1)] float crouchSpeedModfier = 1;
        [SerializeField] float crouchCollisionHeight;
        [Space]
        [SerializeField] Animator useAnimator;

        CharacterController controller;

        private float originalCollisionHeight;

        private float inputFwSpeed;
        private float currentTorque;
        private float ySpeed;

        public Vector3 currentMoviment { get;  private set; }
        public bool isGrounded => controller.isGrounded;
        public bool isCrouching { get; private set; }

        private void Awake()
        {
            controller = GetComponent<CharacterController>();
            originalCollisionHeight = controller.height;
        }

        public void SetSpeed(float fowardValue, float torqueValue)
        {
            if (controller.isGrounded || onAirControll == 1)
            {
                inputFwSpeed = fowardValue;
                currentTorque = (torqueValue * turnSpeed);
            }
            else
            {
                inputFwSpeed += fowardValue * (onAirControll / 50);
                inputFwSpeed = Mathf.Clamp(inputFwSpeed, -1, 1);

                currentTorque = (torqueValue * turnSpeed) * onAirControll;
            }
        }

        public void Jump()
        {
            if (controller.isGrounded)
            {
                ySpeed = jumpStrength * Mathf.Abs(Physics.gravity.y / 3);
            }
        }

        public void Crouch(bool crouch)
        {
            if (isCrouching == crouch || (crouch) && controller.isGrounded == false)
            {
                return;
            }

            controller.height = crouch ? crouchCollisionHeight : originalCollisionHeight;
            Vector3 targetCenter = controller.center;
            targetCenter.y = controller.height / 2;
            controller.center = targetCenter;
            isCrouching = crouch;
        }

        private void Update()
        {
            if (!controller.enabled)
                return;

            currentMoviment = GetMovimentFormula();
            controller.Move(currentMoviment * Time.deltaTime);
            transform.Rotate(Vector3.up, currentTorque * (Time.deltaTime * 100));

            ApplyGravity();
        }

        Vector3 GetMovimentFormula()
        {
            float fowardMoviment = inputFwSpeed;

            if (isCrouching)
            {
                fowardMoviment *= crouchSpeedModfier;
            }
            else
            {
                fowardMoviment *= fowardMoviment != 0 && fowardMoviment < 0 ? backWalkModf : 1;
            }

            Vector3 currentMoviment = (transform.forward * fowardMoviment) * movimentSpeed;
            currentMoviment.y = ySpeed;
            return currentMoviment;
        }

        void ApplyGravity()
        {
            if (controller.isGrounded == false)
            {
                ySpeed += (Physics.gravity.y * 2) * Time.deltaTime;
                Crouch(false);
            }
            else
            {
                ySpeed = -0.5f;
            }
        }
    }
}

