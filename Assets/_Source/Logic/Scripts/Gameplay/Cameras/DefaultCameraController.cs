using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest.Cameras
{
    public class DefaultCameraController : MonoBehaviour
    {
        CinemachineOrbitalTransposer transposer;

        private void Awake()
        {
            transposer = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineOrbitalTransposer>();
        }

        public void ChangeSensibility(DataValueFloat sensibility)
        {
            ChangeSensibility(sensibility.value);
        }

        public void ChangeSensibility(float sensibility)
        {
            transposer.m_XAxis.m_MaxSpeed = sensibility;
        }

    }
}