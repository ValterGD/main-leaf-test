using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainLeafTest.Cameras
{
    public class VcamActiveZone : MonoBehaviour
    {
        [SerializeField] CinemachineVirtualCamera vcam;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                vcam.Priority = 30;
                vcam.LookAt = other.transform;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                vcam.Priority = -30;
            }
        }
    }
}
