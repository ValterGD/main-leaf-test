﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MainLeafTest.Tools;

namespace MainLeafTest.IA
{
    public class WaypointMoviment : MonoBehaviour
    {
        [Header("Moviment")]
        [SerializeField] float moveSpeed = 3;
        [SerializeField] private bool dontMoveOnY = true;
        [Header("Rotation")]
        [SerializeField] bool rotateOnMove = true;
        [SerializeField] float rotateTime = 0.25f;
        [Space]
        [SerializeField] List<Transform> waypoints;

        Transform lastWaypoint;
        Transform nextWaypoint => GetNextWaypoint();

        public void ResetPatrolPosition()
        {
            var firstWaypoint = waypoints[0];
            transform.position = firstWaypoint.position;
            lastWaypoint = firstWaypoint;

            if (rotateOnMove)
                LookToWaypoint(nextWaypoint.position, lookTime: 0f);
        }

        public Transform GetNextWaypoint()
        {
            int previousWaypointIndex = waypoints.IndexOf(lastWaypoint);
            int nextWaypointIndex = previousWaypointIndex + 1;
            nextWaypointIndex = nextWaypointIndex >= waypoints.Count ? 0 : nextWaypointIndex;

            return waypoints[nextWaypointIndex];
        }

        public void GoToNextWaypoint(UnityAction onMovimentFinished)
        {
            Action onCompleted = () =>
            {
                lastWaypoint = nextWaypoint;
                onMovimentFinished?.Invoke();
            };

            Vector3 targetPosition = dontMoveOnY ? new Vector3(nextWaypoint.position.x, transform.position.y, nextWaypoint.position.z) : transform.position;
            transform.TweenMove(targetPosition, moveSpeed, onCompleted);

            if (rotateOnMove)
                LookToWaypoint(nextWaypoint.position);
        }

        public void LookToWaypoint(Vector3 nextPoint)
        {
            LookToWaypoint(nextPoint, rotateTime);
        }
        public void LookToWaypoint(Vector3 nextPoint, float lookTime)
        {
            Vector3 delta = nextPoint - transform.position;
            float angle = Quaternion.LookRotation(delta, Vector3.up).eulerAngles.y;

            Quaternion targetRotation = Quaternion.Euler(new Vector3(transform.eulerAngles.x, angle, transform.eulerAngles.z));
            transform.TweenRotation(targetRotation, lookTime);
        }

        public void StopMoviment()
        {
            transform.StopAllTweens();
        }
    }
}