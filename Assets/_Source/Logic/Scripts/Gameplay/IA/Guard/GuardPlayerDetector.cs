﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MainLeafTest.Architecture;

namespace MainLeafTest.IA
{
    public class GuardPlayerDetector : MonoBehaviour
    {
        [SerializeField] LayerMask detectionLayer;
        public GlobalEvent onPlayerDetected;
        static bool playerDetected = false;

        public void EnableDetection(bool enable)
        {
            playerDetected = !enable;
        }
        private void OnTriggerStay(Collider other)
        {
            if (playerDetected)
            {
                return;
            }
            if (other.CompareTag("Player") && CheckIfVisionIsFreeTo(other.transform))
                PlayerDetected();
        }
        public void PlayerDetected()
        {
            if (playerDetected)
            {
                return;
            }
            playerDetected = true;
            onPlayerDetected?.Invoke();
        }

        private bool CheckIfVisionIsFreeTo(Transform objectToFind)
        {
            Vector3 dir = (objectToFind.position - transform.position).normalized;
            float distance = Vector3.Distance(objectToFind.position, transform.position) * 1.25f;
            Physics.Raycast(transform.position, dir, out RaycastHit visionHit, detectionLayer);
            return (visionHit.transform == objectToFind);
        }
    }
}
