﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest.IA
{
    public class GuardController : MonoBehaviour
    {
        [Header("IA")]
        [SerializeField] DataValueGameObject playerReference;

        GuardAnimations guardAnimations;
        WaypointMoviment movimentController;

        private void Awake()
        {
            guardAnimations = GetComponent<GuardAnimations>();
            movimentController = GetComponent<WaypointMoviment>();
        }

        public void ResetGuard()
        {
            movimentController.StopMoviment();
            guardAnimations.StopLookAround();

            movimentController.ResetPatrolPosition();
        }

        public void StartPatroling()
        {
            PlayPatrolLooking();
        }

        private void PlayPatrolLooking()
        {
            guardAnimations.AnimateWalk(false);
            guardAnimations.PlayLookAround(onLookFinished: PlayPatrolWalking);
        }
        private void PlayPatrolWalking()
        {
            guardAnimations.AnimateWalk(true);
            movimentController.GoToNextWaypoint(onMovimentFinished: PlayPatrolLooking);
        }
        private void StopPatroll()
        {
            movimentController.StopMoviment();
            guardAnimations.StopLookAround();
            guardAnimations.AnimateWalk(false);
        }

        public void LookAtPlayer()
        {
            StopPatroll();
            movimentController.LookToWaypoint(playerReference.value.transform.position, 1f);
        }
    }
}
