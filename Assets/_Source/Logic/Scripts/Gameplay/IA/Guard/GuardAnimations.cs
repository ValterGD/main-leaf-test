﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MainLeafTest.IA
{
    public class GuardAnimations : MonoBehaviour
    {
        [Header("LookAt")]
        [SerializeField] AnimationClip lookAroundClip;
        [SerializeField] string lookAroundBool = "LookTru";
        [HideInInspector] public UnityAction onLookCompleted;
        [Header("Walk")]
        [SerializeField] string walkBool = "Walking";

        private Animator animator;

        void Awake()
        {
            animator = GetComponent<Animator>();
            TrySetupLookAroundEvent();
        }
        private void TrySetupLookAroundEvent()
        {
            if (lookAroundClip.events.Length < 1)
            {
                var lookAroundEvent = new AnimationEvent()
                {
                    functionName = "OnLookAroundCompleted",
                    time = lookAroundClip.length
                };

                lookAroundClip?.AddEvent(lookAroundEvent);
            }
        }
        public void PlayLookAround(UnityAction onLookFinished)
        {
            animator.SetBool(lookAroundBool, true);
            onLookCompleted = onLookFinished;
        }
        public void StopLookAround()
        {
            onLookCompleted = null;
            animator.SetBool(lookAroundBool, false);
        }
        public void OnLookAroundCompleted()
        {
            animator.SetBool(lookAroundBool, false);
            onLookCompleted?.Invoke();
        }

        public void AnimateWalk(bool walking) 
        {
            animator.SetBool(walkBool, walking);
        }
    }
}