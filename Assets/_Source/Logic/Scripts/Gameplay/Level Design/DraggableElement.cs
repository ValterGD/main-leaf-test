using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest
{
    public class DraggableElement : MonoBehaviour
    {
        [SerializeField] GlobalEvent onDragIsEnable;
        [SerializeField] GlobalEvent onDragIsDisable;
        [Space]
        [SerializeField] DataValueGameObject currentDragable;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && currentDragable.value == null)
            {
                currentDragable.value = this.gameObject;
                onDragIsEnable?.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player") && currentDragable.value == gameObject)
            {
                currentDragable.value = null;
                onDragIsDisable?.Invoke();
            }
        }
    }
}
