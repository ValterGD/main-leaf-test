using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest
{
    public class WinZone : MonoBehaviour
    {
        [SerializeField] GameStateScript gameWinState;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                gameWinState?.ActivateState();
            }
        }
    }
}
