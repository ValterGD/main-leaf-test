using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest
{
    public class TeleportController : MonoBehaviour
    {
        [SerializeField] Vector3 teleportOffeset = Vector3.up;
        [SerializeField] GlobalEvent onObjectSpawned;

        public void TeleportObjectHere(DataValueGameObject toTeleport)
        {
            TeleportObjectHere(toTeleport.value.transform);
        }
        public void TeleportObjectHere(Transform toTeleport)
        {
            toTeleport.position = transform.position + teleportOffeset;
            toTeleport.rotation = transform.rotation;

            onObjectSpawned?.DelayedInvoke(Time.fixedDeltaTime);
        }
    }
}
