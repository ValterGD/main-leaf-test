using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainLeafTest.Architecture;

namespace MainLeafTest
{
    public class Ruppie : MonoBehaviour
    {
        [SerializeField] GlobalEvent onPickUp;

        Collider colliderTrigger;

        private void Awake()
        {
            colliderTrigger = GetComponentInChildren<Collider>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                onPickUp?.Invoke();
                colliderTrigger.gameObject.SetActive(false);
            }

        }

    }
}
