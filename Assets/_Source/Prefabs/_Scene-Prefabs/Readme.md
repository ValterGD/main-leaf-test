Scene prefabs are used to ensure the design team can work in parallel by changing only the modified prefabs instead of having everyone changing the whole scene.
They also reduce the granularity of files (both scene and prefab).
Using Additive Scene load is preferred whenever possible, thus separating a big scene into smaller scenes.