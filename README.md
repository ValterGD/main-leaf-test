Versão principal do teste técnico. Está pronto para download tanto a build quanto o projeto em si. Eu ainda devo fazer algumas coisas como organização de código, organização do projeto, polimentos, e comentários relevantes.

Mas devido eu ter levado mais tempo que o previsto pois estava ocupado decidi disponibilizar logo uma primeira versão.

Atenção: Eu acabei adicionando a mecânica de agachar manualmente, através da tecla CRTL. Essa funcionalidade não estava previsata no manual e só reparei depois. Os outros controles seguem conforme as instruções originais.
